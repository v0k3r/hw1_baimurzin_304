-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"

instance Show' A where
  show' = undefined

instance Show' C where
  show' = undefined

instance Show' Int where
  show' a | a < 0 = "(" ++ show a ++ ")"
          | True  = show a

instance Show' A where
  show' B = "B"
  show' (A a) = "A " ++ show' a

instance Show' C where
  show' (C a b) = "C " ++ show' a ++ " " ++ show' b

----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}

symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set f1) (Set f2) = Set $ \x -> (f1 x) && (not $ f2 x) || (f2 x) && (not $ f1 x)

-----------------

-- fromBool - переводит булевское значение в кодировку Чёрча
fromBool True = \t -> \f -> t
fromBool False = \t f -> f

-- fromInt - переводит число в кодировку Чёрча
fromInt n = \s z -> ( if n == 0 then z else s (fromInt(n-1) s z)  )
